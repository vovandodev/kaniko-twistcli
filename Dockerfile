FROM alpine

COPY twistcli /usr/local/bin/twistcli 
RUN  chmod a+x /usr/local/bin/twistcli && export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin

COPY --from=gcr.io/kaniko-project/executor:debug /kaniko/executor /kaniko/executor
